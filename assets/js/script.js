'use strict';

$(function () {

    window.onscroll = function() {scrollFunction()};
    function scrollFunction() {
        if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
            document.getElementById("back-to-top").style.display = "block";
        } else {
            document.getElementById("back-to-top").style.display = "none";
        }
    }

    $('.menu-btn').on('click', function (e) {
        e.stopPropagation();
        $('.items').toggleClass('items-hide');
        $('.items-overlay').toggleClass('show');
    });

    $(".items-overlay").on('click', function(){
        $(".items").removeClass('items-hide');
        $('.items-overlay').removeClass('show');
    });

    $('#back-to-top').on('click',function() {
        $('body, html').animate({
            scrollTop : 0
        }, 300);
    });

    $('a.i[href^="#"]').on('click',function(event) {
        let id = $(this).attr("href");
        let offset = 80;
        let target = $(id).offset().top - offset;
        $('html, body').animate({scrollTop:target}, 300);
        event.preventDefault();
    });

    const swiper = new Swiper('.swiper-container', {
        autoplay: {
            delay: 5000
        },
        loop: true,
        speed: 1000,
        effect: 'fade',
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        }
    });

    let lastId,
        topMenu = $(".items"),
        topMenuHeight = topMenu.outerHeight()+1,
        menuItems = topMenu.find("a.o"),
        scrollItems = menuItems.map(function(){
            let item = $($(this).attr("href"));
            if (item.length) { return item; }
        });

    menuItems.on('click',function(e){
        let href = $(this).attr("href"),
            offsetTop = href === "#" ? 0 : $(href).offset().top-80;
        $('html, body').stop().animate({
            scrollTop: offsetTop
        }, 300);
        e.preventDefault();
    });

    $(window).on('scroll',function(){
        let fromTop = $(this).scrollTop()+topMenuHeight;
        let cur = scrollItems.map(function(){
            if ($(this).offset().top < fromTop)
                return this;
        });
        cur = cur[cur.length-1];
        let id = cur && cur.length ? cur[0].id : "";
        if (lastId !== id) {
            lastId = id;
            menuItems
                .parent().removeClass("active")
                .end().filter("[href='#"+id+"']").parent().addClass("active");
        }
    });

});