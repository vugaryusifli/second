'use strict';

$(function () {

    let btn = document.getElementsByClassName("md");
    let close = document.getElementsByClassName("close");

    for (let i = 0; i < btn.length; i++) {
        let thisBtn = btn[i];
        thisBtn.addEventListener("click", function(){
            let modal = document.getElementById(this.dataset.modal);
            modal.style.display = "-webkit-box";
            modal.style.display = "-webkit-flex";
            modal.style.display = "-moz-box";
            modal.style.display = "-ms-flexbox";
            modal.style.display = "flex";
            document.body.classList.add("modal-open");
        }, false);
    }

    for (let i = 0; i < close.length; i++) {
        let thisClose = close[i];
        thisClose.addEventListener("click", function () {
            let modal = document.getElementById(this.dataset.modal);
            modal.style.display = "none";
            document.body.classList.remove("modal-open");
        }, false);
    }

});